# Termómetro-IR

## Descripción
Desarrollo de un termómetro digital infrarrojo para uso médico. Basado en Arduino y el sensor
infrarrojo de temperatura [MLX90614](/docs/datasheet_mlx90614.pdf). La versión MLX90614DAA está
diseñada para uso médico, con una precisión de 0.1°C en el rango de 36°C a 39°C.
