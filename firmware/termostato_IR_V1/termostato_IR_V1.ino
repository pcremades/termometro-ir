
/* Copyright 2020 Eduardo Sanabria
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <EEPROM.h>
#include <LiquidCrystal.h>
#include <Wire.h>
#include <Adafruit_MLX90614.h>

Adafruit_MLX90614 mlx = Adafruit_MLX90614();

LiquidCrystal lcd(12, 11, 5, 4, 3, 2); // lcd en pines 12,11,5,4,3,2

void setup() {
  
  Serial.begin (9600); 
  lcd.begin(16, 2); 
  lcd.setCursor(0,0); 
  lcd.print("Termometro IR"); 
  lcd.setCursor(0,1); 
  lcd.print("version 1"); 
  delay (3000);
  lcd.clear(); 
  lcd.setCursor(0,0); 
  lcd.print("IP COVID 19"); 
  lcd.setCursor(0,1);
  lcd.print("Iniciando espere"); 
  delay (3000); 
  lcd.clear(); 

 mlx.begin();
 delay(1000);
 Serial.print("Emisividad: ");
 Serial.println(mlx.readEmmisivity());
 delay(1000);
}
void loop() {

  
lcd.setCursor (0,0); 
lcd.print ("Tempetatura: "); 
lcd.setCursor (4,1);
lcd.print (mlx.readObjectTempC());

delay (500); 



} 
